/**
 * Created with IntelliJ IDEA.
 * User: alec
 * Date: 10/3/13
 * Time: 6:13 AM
 * To change this template use File | Settings | File Templates.
 */
import java.io.*;
import java.util.Vector;

public class Main {

    public static boolean testing = false; //Set to false when submitting to kattis
    public static boolean runTests = false; //Set to true to run "unit tests"
    private static int passCount =0;
    public static void main(String[] args) throws IOException {

    Vector<String> board = new Vector<String>();

       BufferedReader br;
       if(!testing)
       {
           br = new BufferedReader(new InputStreamReader(System.in));
       }
       else
       {
            br = new BufferedReader(new FileReader("maps/test005.in"));
//          br = new BufferedReader(new FileReader("maps/37.txt"));
       }

    String line;
    while(br.ready()) {
      line = br.readLine();
      board.add(line);
    } //End while
    
    if(runTests)
    {
        runTests();
        return;
    }
    else if (testing)
    {

        BoardState bs = new BoardState(board, null);
        bs.printBoard();
//        bs.printDistanceToGoals();
//        runAStar(bs);
        runGreedy(bs);
    }
    else
    {
       BoardState bs = new BoardState(board,null);
//        bs.printBoard();
       runGreedy(bs);
    }
  }//main

    public static long runAStar(BoardState bs)
    {

        String answer;
        long startTime;
        long endTime;
        SokobanSolver solver = new SokobanSolver();
        startTime = System.nanoTime();
        answer = solver.solve(bs);
        System.out.println(answer);
        endTime = System.nanoTime();
        long astarTime = endTime - startTime;
        System.out.println("TIME ASTAR= " + (astarTime * Math.pow(10,-6)));
        System.out.println("Nodes Visited= " + solver.nodeCount);
        System.out.println("NanoSeconds/node= " + astarTime/solver.nodeCount);
        return endTime - startTime;
    }

    public static long runGreedy(BoardState bs)
    {
        String answer;
        long startTime;
        long endTime;
        SokobanSolver solver = new SokobanSolver();
        startTime = System.nanoTime();
        answer = solver.solveWithPriorityQueue(bs);
        endTime = System.nanoTime();
        if (testing)
        {
            System.out.println(answer);
            long greedyTime = endTime - startTime;
            System.out.println("TIME GREEDY= " + (greedyTime * Math.pow(10,-6)));
            System.out.println("Nodes Visited= " + solver.nodeCount);
            System.out.println("NanoSeconds/node= " + greedyTime/solver.nodeCount);
        }
        else
            System.out.println(answer);
        return endTime - startTime;
    }
    public static long runIDA(BoardState bs)
    {
        String answer;
        long startTime;
        long endTime;
        SokobanSolver solver = new SokobanSolver();
        startTime = System.nanoTime();
        answer = solver.solveWithIDA(bs);
        endTime = System.nanoTime();
        if (testing)
        {
            System.out.println(answer);
            long greedyTime = endTime - startTime;
            System.out.println("TIME IDA= " + (greedyTime * Math.pow(10,-6)));
            System.out.println("Nodes Visited= " + solver.nodeCount);
            System.out.println("NanoSeconds/node= " + greedyTime/solver.nodeCount);
        }
        else
            System.out.println(answer);
        return endTime - startTime;
    }

    private static void runTests()
    {
        new BoardIteratorTest().runTests();
        new BoardStateTest().runTests();
        new ChildStatesTest().runTests();
        //new SokobanSolverTest().runTests();
        runAllTests();
    }

    private static void runAllTests()
    {
        for(int i=0;i<100;i++)
            runTestNumber(i);
        System.out.println(passCount);
    }

    private static void runTestNumber(int i)
    {
        BufferedReader br;
        try{
            br = new BufferedReader(new FileReader("maps/test" + String.format("%03d", i) + ".in"));
            Vector<String> board = new Vector<String>();
            String line;
            while(br.ready()) {
                line = br.readLine();
                board.add(line);
            }

            testBoard(new BoardState(board,null), i);
        }catch (FileNotFoundException e )
        {
            return;
        }catch (IOException e)
        {
            return;
        }

    }

    private static void testBoard(BoardState bs, int mapNum)
    {
//        bs.printBoard();
        SokobanSolver solver = new SokobanSolver();
        String answer = solver.solveWithPriorityQueue(bs);
        if (answer.equals("NO SOLUTIONNNNNN"))
            System.out.println("[FAIL] - Map " + mapNum + " " + answer);
        else if (answer.equals("TIMEOUT"))
            System.out.println("[FAIL] - Map " + mapNum + " " + answer);
        else
        {

            System.out.println("[PASS] - Map " + mapNum);
            passCount++;
        }
    }
}//End Main
