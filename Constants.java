/**
 * Created with IntelliJ IDEA.
 * User: alec
 * Date: 10/4/13
 * Time: 3:38 AM
 * To change this template use File | Settings | File Templates.
 */
public class Constants {

    private Constants(){};

    /**
     * ' ' - Open Space
     * '#' - Wall
     * '.' - Goal
     * '$' - Box
     * '*' - Box on Goal
     * '+' - Player on Goal
     **/

    public final static char PLAYER = '@';
    public final static char WALL = '#';
    public final static char GOAL = '.';
    public final static char BOX = '$';
    public final static char BOX_ON_GOAL = '*';
    public final static char PLAYER_ON_GOAL = '+';
    public final static char POSSIBLE_PLACE_TO_GO = '^'; //Places where the player is able to reach
    public final static char OPEN_SPACE = ' ';
    public final static char DEAD_ZONE = '!'; //A box cannot go here, if it does we are in a deadlock
    public final static char TERMINATING_CHAR = 00;
    public final static char INDEX_OUT_OF_BOUNDS_CHAR = 255;


}
