import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: alec
 * Date: 10/9/13
 * Time: 11:11 AM
 * To change this template use File | Settings | File Templates.
 */
public class PointUtil {

    public static Point pointAbove(Point point)
    {
       return new Point(point.x, point.y - 1);
    }
    public static Point pointBelow(Point point)
    {
        return new Point(point.x, point.y + 1);

    }
    public static Point pointRight(Point point)
    {
        return new Point(point.x + 1, point.y);
    }
    public static Point pointLeft(Point point)
    {
        return new Point(point.x - 1, point.y);
    }
}
