import java.text.StringCharacterIterator;
import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: alec
 * Date: 10/7/13
 * Time: 5:58 AM
 * To change this template use File | Settings | File Templates.
 */
public class BoardIteratorTest {

    public void runTests()
    {
       testNextPoint();
    }

    public void testNextPoint() {

        // ########
        // #      #
        // #  . ###
        // #@####
        // ######
        //
        Vector<String> boardState = new Vector<String>();
        boardState.add("########");
        boardState.add("##     #");
        boardState.add("#  . ###");
        boardState.add("#@####");
        boardState.add("######");
        String singleString = "########" + "###     #" + "#  . ###" + "#@####" + "######";
        StringCharacterIterator stringIterator = new StringCharacterIterator(singleString);
        BoardIterator bi = new BoardIterator(boardState);
        int counter = 0;

        while(true)
        {

            char c = bi.nextPoint();
            char v = stringIterator.next();

            if (c == Constants.TERMINATING_CHAR)
                break;
            if(v != c)
            {
                System.out.println("[FAIL] - Test Board Iterator");
                return;
            }
        }
        System.out.print("[PASS]");

        System.out.println(" - Test Board Iterator");
    }

    public void testSeeUpByAmount() throws Exception {

    }

    public void testSeeDownByAmount() throws Exception {

    }

    public void testSeeLeftByAmount() throws Exception {

    }

    public void testSeeRightByAmount() throws Exception {

    }
}
