import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: alec
 * Date: 10/16/13
 * Time: 5:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class GoalMapping {
    public int[][] map;
    public Point boxPoint;
    public Point goalPoint;
    public int score = -1;

    public void setBoxPoint(Point point)
    {
        boxPoint = point;
        score = map[point.y][point.x];
    }

    @SuppressWarnings("unchecked")
    public GoalMapping clone()
    {
       GoalMapping newMapping = new GoalMapping();
        newMapping.map = this.map.clone();
        newMapping.boxPoint = (Point)this.boxPoint.clone();
        newMapping.score = score;
        newMapping.goalPoint = (Point)goalPoint.clone();
        return newMapping;
    }
}
