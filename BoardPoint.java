import java.awt.*;
import java.util.*;

/**
 * 
 * @author David
 *	The class is used to make it easier to keep track of score,
 *	and to implement sorting of Point's based on their score for A*
 *
 */
public class BoardPoint extends Point implements Comparable<BoardPoint> {
	
	private int g_score, f_score;
	private char sign;
	private BoardPoint parent;
	private BoardState boardState;
	BoardPoint(int x, int y, BoardPoint parent, BoardState boardState)
	{
		super(x, y);
		this.boardState = boardState;
		setParent(parent);
	}
	BoardPoint(Point p, BoardPoint parent, BoardState boardState)
	{
		super(p.x, p.y);
		this.boardState = boardState;
		setParent(parent);
	}
	
	public void setG_Score(int newScore)
	{
		g_score = newScore;
	}
	public int getG_Score() 
	{
		return g_score;
	}
	public void setF_Score(int newScore)
	{
		f_score = newScore;
	}
	public int getF_score() {
		return f_score;
	}
	public void setParent(BoardPoint p)
	{
		parent = p;
	}
	public BoardPoint getParent()
	{
		return parent;
	}
	/**
	 * 
	 * @return - The valid neighbours of this point (based on space for pushing from and pushing to)
	 */
	public Vector<Point> getNeighbours() {
		Vector<Point> neighbours = new Vector<Point>();
		if (x - 1 >= 0 && x + 1 < boardState.board.get(y).length())
		{
			if (boardState.board.get(y).charAt(x-1) != Constants.WALL && boardState.board.get(y).charAt(x+1) != Constants.WALL)
				neighbours.add(new Point(x-1, y));
		}
		if (x + 1 < boardState.board.get(y).length() && x - 1 >= 0)
		{
			if (boardState.board.get(y).charAt(x+1) != Constants.WALL  && boardState.board.get(y).charAt(x-1) != Constants.WALL)
				neighbours.add(new Point(x+1, y));
		}
		if (y - 1 >= 0 && y + 1 < boardState.board.size())
		{
			if (boardState.board.get(y-1).charAt(x) != Constants.WALL  && boardState.board.get(y+1).charAt(x) != Constants.WALL)
				neighbours.add(new Point(x, y-1));
		}
		if (y - 1 >= 0 && y + 1 < boardState.board.size()) 
		{
			if (boardState.board.get(y+1).charAt(x) != Constants.WALL  && boardState.board.get(y-1).charAt(x) != Constants.WALL)
				neighbours.add(new Point(x, y+1));
		}
		return neighbours;
	}
	public Vector<BoardPoint> reconstructPath() {
		Vector<BoardPoint> path = new Vector<BoardPoint>();
		BoardPoint bp = this;
		path.add(bp);
		while (bp.parent != null)
		{
			bp = bp.parent;
			path.add(bp);
		}
		Collections.reverse(path);
		return path;
	}
	public Point toPoint() {
		return new Point(this.x, this.y);
	}
	public int compareTo(BoardPoint p)
	{
		if (getF_score() > p.getF_score())
		{
			return 1;
		}
		else if (getF_score() < p.getF_score())
		{
			return -1;
		}
		else return 0;
	}
	public String toString() {
		return "("+x+", "+y+")";
	}
}
