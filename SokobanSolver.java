import java.awt.*;
import java.util.Collections;
import java.util.HashMap;
import java.util.PriorityQueue;
import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: alec
 * Date: 10/3/13
 * Time: 6:20 AM
 * To change this template use File | Settings | File Templates.
 */


/**
 * A class to solve the Sokoban problem
 */
public class SokobanSolver {

    public int nodeCount = 0;
    private final long allowableRunTime = 550;
    private long startTime;

   /**
    * 
    * @param bs - the root boardstate which we want to solve
    * @return - An array of boardstates if there are any solution, otherwise null if there arent any
    */
   public String solve(BoardState bs)
   {
       startTime = System.nanoTime();

	   Vector<BoardState> openSet = new Vector<BoardState>();
	   HashMap<String, BoardState> goalMappingHash = new HashMap<String, BoardState>();
	   HashMap<String, BoardState> closedSet = new HashMap<String, BoardState>();
	   BoardState current = null;

	   bs.setGScore(0);
	   bs.setFScore(bs.score);
	   bs.setHeuristicScore();
	   openSet.add(bs);
	   
	   while (!openSet.isEmpty())
	   {
		   Collections.sort(openSet);
		   current = openSet.firstElement();
		   if (current.score == 0)
		   {
                System.out.println("Solution");
               current.printBoard();
			   return current.movesToReachState;
		   }
		   
		   openSet.removeElementAt(0);
		   closedSet.put(current.hashString, current);
		   
		   current.updateChildStates();
		   Vector<BoardState> childStates = current.childStates;
		   for (BoardState child : childStates)
		   {
			   int tentative_gScore = current.gScore + 1;
			   int tentative_fScore = tentative_gScore + child.score;
               //TODO::This isn't right
			   BoardState cs = closedSet.get(child.getBoardWithoutPlayer());
			   if (cs != null && tentative_fScore >= cs.getFScore() )
			   {
				   continue;
			   }
			   if (!goalMappingHash.containsKey(child.hashString) || tentative_fScore < cs.getFScore())
			   {
				   if (cs == null)
				   {
					   cs = child;
				   }
				   cs.setParent(current);
				   cs.setGScore(tentative_gScore);
				   cs.setFScore(tentative_fScore);
				   if (!goalMappingHash.containsKey(cs.hashString))
				   {
					   closedSet.remove(cs.getBoardWithoutPlayer());
					   openSet.add(cs);
					   goalMappingHash.put(current.hashString, child);
				   }
			   }
			   
		   }
		   //System.out.println("The openset size: "+openSet.size());
	   }
	   return "No path found";
   }

    public String solveWithPriorityQueue(BoardState boardState)
    {
    	int counter = 0;
//        startTime = System.nanoTime();
        //HashMap<String, BoardState> hashMap = new HashMap<String, BoardState>();
        HashMap<Vector<String>, BoardState> hashMap = new HashMap<Vector<String>, BoardState>();
    	PriorityQueue<BoardState> queue = new PriorityQueue<BoardState>();
    	boardState.setHeuristicScore();
        queue.add(boardState);
        //boardState.printBoard();
        while(!queue.isEmpty())
        {
//            if ((System.nanoTime() - startTime) * Math.pow(10,-6) > allowableRunTime)
//                return "TIMEOUT";
            BoardState currentState = queue.poll();
            //currentState.printBoardWithParent();
            hashMap.put(currentState.board, currentState);
            if(currentState.score == 0)
            {
//                Vector<BoardState> path = currentState.reconstructPath();
//                for(BoardState state : path)
//                    state.printBoard();
//                Vector<BoardState> bss = currentState.reconstructPath();
//                for(BoardState state : bss)
//                    state.printBoard();
//    			Vector<BoardState> goalPath = currentState.reconstructPath();
//    			for (BoardState bs : goalPath)
//    			{
//    				bs.printBoard();
//    				System.out.println();
//    			}
            	
                return currentState.movesToReachState;
            }
            else
            {
                currentState.updateChildStates();
                int hashedAway = 0;
                for(BoardState childState : currentState.childStates)
                {
                    if(hashMap.containsKey(childState.board))
                    {
                    	hashedAway++;
                        continue;
                    }
                    queue.add(childState);
                    nodeCount++;
                }
                
//                System.out.println("Queue after iteration nr: "+counter+" with queue length "+queue.size()+" and "+hashedAway+" ones thrown away due to hash");
//                queue.peek().printBoard();
////                for (BoardState qs : queue)
////                {
////                	System.out.print(qs.score+" ");
////            	}
//                System.out.println();
//                System.out.println();
//                if (counter == 50)
//                	break;
//                
//                
                counter++;
            
            }
        }
        return "NO SOLUTIONNNNNN";
    }
    public String solveWithIDA(BoardState root)
    {
    	root.setGScore(0);
    	root.setHeuristicScore();
    	root.setFScore(root.score);
    	BoardState t = root;
    	while (true)
    	{
    		t = search(t, t.getGScore(), t.getFScore());
    		
    		if (t.isGoalState)
    		{
    			return t.movesToReachState;
    		}
    		else if (t.getFScore() == Integer.MAX_VALUE)
    			return "not found";
    		
    	}
    	
    }

    private BoardState search(BoardState root, int g, int bound)
    {
    	int f = g + root.score;
    	if (f > bound) {
    		root.setGScore(g);
    		root.setFScore(f);
    		return root;
    	}
    	if (root.score==0)
    	{
    		root.isGoalState = true;
    		return root;
    	}
    	int minScore = Integer.MAX_VALUE;
    	BoardState min = new BoardState();
    	min.setFScore(minScore);
    	root.updateChildStates();
    	for (BoardState child : root.childStates)
    	{
    		BoardState t = search(child, g + 1, bound);
    		if (t.isGoalState)
    			return t;
    		if (t.getFScore() < minScore)
    		{
    			minScore = t.getFScore();
    			min = t;
    		}
    	}
    	return min;
    }
}
