import java.awt.*;
import java.util.Vector;

public class BoardIterator{

    public int x = -1;
    public int y = 0;
    private Vector<String> boardState;

    public BoardIterator(Vector<String> boardState)
    {
       this.boardState = boardState;
    }
    //TODO: Add Error Checks to this
    public char nextPoint()
    {
        x++;

        if(x >= boardState.get(y).length())
        {
            if(y == boardState.size() - 1)
                return Constants.TERMINATING_CHAR;
            x=0;
            y++;
        }
        return getAtPoint(new Point(x, y));
    }

    public void setLocation(Point point)
    {
        x = point.x;
        y = point.y;
    }

    public char currentChar()
    {
        return getAtPoint(new Point(x,y));
    }

    public char seeUp()
    {
        return seeUpByAmount(1);
    }

    public char seeDown()
    {
        return seeDownByAmount(1);
    }

    public char seeLeft()
    {
        return seeLeftByAmount(1);
    }

    public char seeRight()
    {
        return seeRightByAmount(1);
    }

    public char seeDirectionByAmount(int direction, int distance)
    {
        if (direction == BoardState.UP)
            return seeUpByAmount(distance);
        if (direction == BoardState.DOWN)
            return seeDownByAmount(distance);
        if (direction == BoardState.LEFT)
            return seeLeftByAmount(distance);
        if (direction == BoardState.RIGHT)
            return seeRightByAmount(distance);
        else
            return 00;
    }

    //The point value is how many places from where you are at to "Move" to see
    public char seeMultiDirection(Point point)
    {
        return getAtPoint(new Point(x+point.x, y+point.y));
    }

    public char seeUpByAmount(int distance)
    {
        return getAtPoint(new Point(x, y-distance));
    }

    public char seeDownByAmount(int distance)
    {
        return getAtPoint(new Point(x, y+distance));
    }

    public char seeLeftByAmount(int distance)
    {
        return getAtPoint(new Point(x-distance, y));
    }

    public char seeRightByAmount(int distance)
    {
        return getAtPoint(new Point(x+distance, y));
    }

    public char getAtPoint(Point point)
    {
       if (point.y < 0 || point.y > boardState.size() - 1 || point.x < 0 || point.x > boardState.get(point.y).length() - 1)
           return Constants.INDEX_OUT_OF_BOUNDS_CHAR;
       return boardState.get(point.y).charAt(point.x);
    }
}