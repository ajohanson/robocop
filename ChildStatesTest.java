import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: alec
 * Date: 10/10/13
 * Time: 8:47 AM
 * To change this template use File | Settings | File Templates.
 */
public class ChildStatesTest {

    public void runTests()
    {
        testMap31();
        testMap32();
    }

    public void testMap31()
    {
       Vector<String> board = new Vector<String>();
       board.add("  #####");
       board.add("  #   #");
       board.add("  #   #");
       board.add("### * ###");
       board.add("#  *.*  #");
       board.add("#  *.*  #");
       board.add("#  *.*  #");
       board.add("###$$$###");
       board.add("  # @ #");
       board.add("  #####");

       Vector<BoardState> childBoards = new Vector<BoardState>();

        Vector<String>child1 = new Vector<String>();
        child1.add("  #####");
        child1.add("  #   #");
        child1.add("  #   #");
        child1.add("### * ###");
        child1.add("#  *.*  #");
        child1.add("#  *.*  #");
        child1.add("#  ***  #");
        child1.add("###$@$###");
        child1.add("  #   #");
        child1.add("  #####");

        BoardState childBoard1 = new BoardState(child1, null);
        childBoards.add(childBoard1);

       BoardState parentBoard = new BoardState(board, null);

       compareChildBoards(parentBoard, childBoards, false, " - map31");

    }

    public void testMap32()
    {
        Vector<String> board = new Vector<String>();
        board.add(" ###########");
        board.add("##         ##");
        board.add("#  $     $  #");
        board.add("# $# #.# #$ #");
        board.add("#    #*#    #####");
        board.add("#  ###.###  #   #");
        board.add("#  .*.@.*.      #");
        board.add("#  ###.###  #   #");
        board.add("#    #*#    #####");
        board.add("# $# #.# #$ #");
        board.add("#  $     $  #");
        board.add("##         ##");
        board.add(" ###########");

        Vector<BoardState> childBoards = new Vector<BoardState>();

        Vector<String> child1 = new Vector<String>();
        child1.add(" ###########");
        child1.add("##         ##");
        child1.add("#  $     $  #");
        child1.add("# $# #.# #$ #");
        child1.add("#    #*#    #####");
        child1.add("#  ###.###  #   #");
        child1.add("#  *+. .*.      #");
        child1.add("#  ###.###  #   #");
        child1.add("#    #*#    #####");
        child1.add("# $# #.# #$ #");
        child1.add("#  $     $  #");
        child1.add("##         ##");
        child1.add(" ###########");

        BoardState childBoard1 = new BoardState(child1, null);
        childBoards.add(childBoard1);

        Vector<String> child2 = new Vector<String>();
        child2.add(" ###########");
        child2.add("##         ##");
        child2.add("#  $     $  #");
        child2.add("# $# #.# #$ #");
        child2.add("#    #*#    #####");
        child2.add("#  ###.###  #   #");
        child2.add("#  *+. .*.      #");
        child2.add("#  ###.###  #   #");
        child2.add("#    #*#    #####");
        child2.add("# $# #.# #$ #");
        child2.add("#  $     $  #");
        child2.add("##         ##");
        child2.add(" ###########");

        BoardState childBoard2 = new BoardState(child2, null);
        childBoards.add(childBoard2);

        Vector<String> child3 = new Vector<String>();
        child3.add(" ###########");
        child3.add("##         ##");
        child3.add("#  $     $  #");
        child3.add("# $# #*# #$ #");
        child3.add("#    #+#    #####");
        child3.add("#  ###.###  #   #");
        child3.add("#  .*. .*.      #");
        child3.add("#  ###.###  #   #");
        child3.add("#    #*#    #####");
        child3.add("# $# #.# #$ #");
        child3.add("#  $     $  #");
        child3.add("##         ##");
        child3.add(" ###########");

        BoardState childBoard3 = new BoardState(child3, null);
        childBoards.add(childBoard3);

        Vector<String> child4 = new Vector<String>();
        child4.add(" ###########");
        child4.add("##         ##");
        child4.add("#  $     $  #");
        child4.add("# $# #.# #$ #");
        child4.add("#    #*#    #####");
        child4.add("#  ###.###  #   #");
        child4.add("#  .*. .*.      #");
        child4.add("#  ###.###  #   #");
        child4.add("#    #+#    #####");
        child4.add("# $# #*# #$ #");
        child4.add("#  $     $  #");
        child4.add("##         ##");
        child4.add(" ###########");

        BoardState childBoard4 = new BoardState(child4, null);
        childBoards.add(childBoard4);


        BoardState parentBoard = new BoardState(board, null);

        compareChildBoards(parentBoard, childBoards, false, " - map32");

    }

    private void compareChildBoards(BoardState boardState, Vector<BoardState> childBoards, boolean printBoards, String descriptor)
    {
        if (printBoards)
        {
            System.out.println("------------");
            System.out.println("Parent Board");
            System.out.println("------------");
            boardState.printBoard();
        }
        boardState.updateChildStates();
        boolean[] childBoardUsed = new boolean[childBoards.size()];
        for (int i=0; i<childBoardUsed.length; i++)
            childBoardUsed[i] = false;

        if (printBoards)
        {
            System.out.println("------------");
            System.out.println("Child Boards");
            System.out.println("------------");
            System.out.println("Number of children: " + boardState.childStates.size());
        }
        for(int i=0; i<boardState.childStates.size(); i++)
        {
            if (printBoards)
            {
               boardState.childStates.get(i).printBoard();
            }
            boolean boardFound = false;
            //check if an unused child state is in the board
            for(int j=0; j<childBoardUsed.length; j++)
            {
                if (childBoardUsed[j] == false && boardState.childStates.get(i).isSameBoard(childBoards.get(j)));
                {
                    childBoardUsed[j] = true;
                    boardFound = true;
                    break;
                }
            }
            if(!boardFound)
            {
                System.out.println("[FAIL] - childBoardsCheck" + descriptor);
            }
        }
        System.out.println("[PASS] - childBoardsCheck" + descriptor);
    }
}
