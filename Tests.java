import java.text.StringCharacterIterator;
import java.util.Vector;

public class Tests {

    public void testIterator(){
        //
        // ########
        // #      #
        // #  . ###
        // #@####
        // ######
        //
        Vector<String> boardState = new Vector<String>();
        boardState.add("########");
        boardState.add("##     #");
        boardState.add("#  . ###");
        boardState.add("#@####");
        boardState.add("######");
        String singleString = "########" + "##     #" + "#  . ###" + "#@####" + "######";
        StringCharacterIterator stringIterator = new StringCharacterIterator(singleString);
        BoardIterator bi = new BoardIterator(boardState);

        while(true)
        {
            char c = bi.nextPoint();
            if (c == 00)
                break;
//            AssertEquals("Iterator.next fail", stringIterator.next(), c);
        }
    }
}



