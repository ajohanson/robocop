import java.util.Vector;



public class SokobanSolverTest {
	
	public void runTests()
	{
		testSolver();
	}
	
	public void testSolver()
	{
    Vector<String> start = new Vector<String>();
    Vector<String> start2 = new Vector<String>();
    Vector<String> start3 = new Vector<String>();

	    start.add("  #####");
	    start.add("  #   #");
	    start.add("  #   #");
	    start.add("### * ###");
	    start.add("#  *.*  #");
	    start.add("#  *.*  #");
	    start.add("#  *.*  #");
	    start.add("###$$$###");
	    start.add("  # @ #");
	    start.add("  #####");
		    

	    start2.add("#########");
	    start2.add("#       #");
	    start2.add("#. # $  #");
	    start2.add("#  .$@  #");
	    start2.add("#       #");
	    start2.add("#########");

	    start3.add("    #####             ");
	    start3.add("    #   #             ");
	    start3.add("    #$  #             ");
	    start3.add("  ###  $###           ");
	    start3.add("  #  $  $ #           ");
	    start3.add("### # ### #     ######");
	    start3.add("#   # ### #######  ..#");
	    start3.add("# $  $             ..#");
	    start3.add("##### #### #@####  ..#");
	    start3.add("    #      ###  ######");
	    start3.add("    ########          ");
	    BoardState bs = new BoardState(start3, null);
//        bs.printDistanceToGoals();
        Main.runGreedy(bs);
//	    SokobanSolver solver = new SokobanSolver();
//	    System.out.println(solver.solve(bs));
	    
	}
}
