import java.util.*;
import java.awt.*;

public class BoardState implements Comparable<BoardState>{
    public static final int UP=0;
    public static final int RIGHT=1;
    public static final int DOWN=2;
    public static final int LEFT=3;
    boolean isGoalState = false;
    int gScore, fScore;
    String movesToReachState = "";
    int longestStringLength; //The longest string to know how wide to make the 2d arrays
	Vector<String> board = new Vector<String>();
    Vector<BoardState> childStates = new Vector<BoardState>();
    //these are [y][x] so that when we print this out it is easier to debug because it will look like the map
    Boolean[][] deadZones;
    String [][] movablePositions;
	Queue<Point> myPointQ = new LinkedList<Point>();
	BoardState parent;
    Vector<GoalMapping> distanceToGoals = new Vector<GoalMapping>();
    int score = Integer.MAX_VALUE;
    String hashString;
    Vector<int[][]>  goalMaps;
    Point[][] goalMapping;

    BoardState(){};

    BoardState(Vector<String> board, BoardState parent)
    {
       this.board = board;

        //find largest string
        for(int i=0; i<board.size();i++)
        {
            if (board.get(i).length() > longestStringLength)
                longestStringLength = board.get(i).length();
        }


       deadZones = new Boolean[board.size()][longestStringLength];
       movablePositions = new String[board.size()][longestStringLength];
       initializeDeadZonesAndMovablePositions();
       findDeadZones();
       createDistanceToGoals();
       getGoalMapping();
       initializeGoalMaps();

    }

    @SuppressWarnings("unchecked")
    public BoardState clone()
    {
        BoardState clone = new BoardState();
        clone.board = (Vector <String>)this.board.clone();
        clone.parent = this;
        clone.goalMapping = this.goalMapping.clone();
        clone.deadZones = this.deadZones.clone();
        clone.childStates = new Vector<BoardState>();
        clone.movablePositions = new String[movablePositions.length][movablePositions[0].length];

        clone.initializeMovablePositions();
        clone.movesToReachState = this.movesToReachState;
        clone.distanceToGoals = new Vector<GoalMapping>();
        for (int i=0;i<this.distanceToGoals.size();i++)
        {
           clone.distanceToGoals.add(distanceToGoals.get(i).clone());
        }
        clone.longestStringLength = this.longestStringLength;
        clone.goalMaps = this.goalMaps;
        clone.score = Integer.MAX_VALUE;
        return clone;
    }

    private String getHash()
    {
       String hashString = "";
        for(GoalMapping mapping : distanceToGoals)
        {
            hashString += mapping.boxPoint.x + "" + mapping.boxPoint.y;
        }
//        System.out.println(hashString);
        this.hashString = hashString;
        return hashString;
    }

    private void initializeDeadZonesAndMovablePositions()
    {
        initializeDeadZones();
        initializeMovablePositions();
    }

    private void initializeDeadZones()
    {
        for(int i=0; i<deadZones.length;i++)
            for(int j=0;j<deadZones[i].length;j++)
                deadZones[i][j] = false;

    }

    private void initializeMovablePositions()
    {
        for(int i=0; i<movablePositions.length;i++)
            for(int j=0;j<movablePositions[i].length;j++)
                movablePositions[i][j] = "";
    }
    
    /**
     * Initializes the goal maps (with the distances) for all the goals
     */
    private void initializeGoalMaps()
    {
    	goalMaps = new Vector<int[][]>();
    	Vector<Point> goalPositions = getGoalPositions();
    	for (Point goal : goalPositions)
    	{
    		goalMaps.add(createGoalMap(goal));
    	}
    }
    
    /** Runs the A* heuristic and returns the score
     * 
     * I just made a really naive/unefficient solution that we can use to test, that works by calculating
     * the sum of the distances (without) walls for each box/goal mapping. It does the mapping every time,
     * so at least it is dynamic.
     * 
     * @return - The score for the current board
     */
    private int getGoalScore()
    {
    	int totalGoalScore = 0;
    	for (int[][] goalMap : goalMaps)
    	{
    		int thisGoalScore = Integer.MAX_VALUE;
    		for (GoalMapping dtg : distanceToGoals)
    		{
    			Point boxPosition = dtg.boxPoint;
    			int newDistance = goalMap[boxPosition.y][boxPosition.x];
//    			int newDistance = (int) Math.pow(goalMap[boxPosition.y][boxPosition.x], 2);
    			if (newDistance < thisGoalScore )
    			{
    				thisGoalScore = newDistance;
    			}
    		}
    		totalGoalScore += thisGoalScore;
    	}
    	
    	return totalGoalScore;
    }
    private int getBoxScore()
    {
    	int totalBoxScore = 0;
    	for (GoalMapping dtg : distanceToGoals)
    	{
    		int thisBoxScore = Integer.MAX_VALUE;
    		Point box = dtg.boxPoint;
    		for (int[][] goalMap : goalMaps)
    		{
    			int newDistance = goalMap[box.y][box.x];
    			//int newDistance = (int) Math.pow(goalMap[box.y][box.x], 2);
    			if (newDistance < thisBoxScore)
    			{
    				thisBoxScore = newDistance;
    			}
    		}
    		totalBoxScore += thisBoxScore;
    	}
    	return totalBoxScore;
    }
    
    
    public void setHeuristicScore() {
    	/*
            if (score != -1)
                return score;
            int score = 0;
            for(GoalMapping mapping : distanceToGoals)
            {
               if (board.get(mapping.boxPoint.y).charAt(mapping.boxPoint.x) == Constants.BOX_ON_GOAL)
                   score-=0;
                else
               {
                   score += mapping.score;
               }
            }
            this.score = score;
            return score;
            */
    	int score = getBoxScore() + getGoalScore();
    	this.score = score;
    }
/*    public int getScore() {
            if (score != -1)
                return score;
            int score = 0;
            boolean finalAnswer = true;
            for(int i=0; i< distanceToGoals.size();i++)
            {
                GoalMapping mapping = distanceToGoals.get(i);
               if (board.get(mapping.boxPoint.y).charAt(mapping.boxPoint.x) == Constants.BOX_ON_GOAL && mapping.score > 0)
               {
                  //We are on a different goal, so swap the goal mapping
                   int indexToSwapWith=-1;
                   for(int j=0;j<distanceToGoals.size();j++)
                   {
                       if(distanceToGoals.get(j).goalPoint.equals(mapping.boxPoint))
                       {
                           indexToSwapWith = j;
                           break;
                       }
                   }

                   Point temp = (Point)mapping.boxPoint.clone();
                   mapping.setBoxPoint((Point)distanceToGoals.get(indexToSwapWith).boxPoint.clone());
                   distanceToGoals.get(indexToSwapWith).setBoxPoint(temp);

                   //Swap GoalMappings, so that the points are in the same place
                   GoalMapping tempMapping = mapping.clone();
                   distanceToGoals.set(i, distanceToGoals.get(indexToSwapWith).clone());
                   distanceToGoals.set(indexToSwapWith, tempMapping);
                   //Recalculate the score
                   score = 0;
                   i=0;
               }
               else
               {
                   score += mapping.score;
                   finalAnswer = false;
               }
            }
            if(finalAnswer)
                return Integer.MIN_VALUE;
            this.score = score;
            return score;
    }*/
	// Get all possible child states
	// Returns a single child if that child is a game winner
	// Does not return a child that is in a deadlock, even if it is a valid move.
	
	public boolean checkIfPlayerCanGo(Point Player) {
		char ch = board.get(Player.y).charAt(Player.x);
		if (ch != Constants.WALL && ch != Constants.BOX && ch != Constants.BOX_ON_GOAL ) { //Checking move validity
		return true;
		}
		return false;
	}

	private void visitNeighbors(Point point, Point player) {
        String currentDirections = movablePositions[point.y][point.x];
        //UP
		Point newPoint = new Point(point.x, point.y-1);
        if(movablePositions[newPoint.y][newPoint.x] == "") //If it's not an empty string it has already been visited
            visitPoint(newPoint, currentDirections + 'U', UP, player);
        //RIGHT
		newPoint = new Point(point.x+1, point.y);
        if(movablePositions[newPoint.y][newPoint.x] == "") //If it's not an empty string it has already been visited
            visitPoint(newPoint, currentDirections + 'R', RIGHT, player);
        //DOWN
		newPoint = new Point(point.x, point.y+1);
        if(movablePositions[newPoint.y][newPoint.x] == "") //If it's not an empty string it has already been visited
            visitPoint(newPoint, currentDirections + 'D', DOWN, player);
        //LEFT
		newPoint = new Point(point.x-1, point.y);
        if(movablePositions[newPoint.y][newPoint.x] == "") //If it's not an empty string it has already been visited
            visitPoint(newPoint, currentDirections + 'L', LEFT, player);
	}

    /**
     * Used in breadthFirstSearch of points
     * @param point - point to visit
     * @param additionalMoves - moves taken to get here
     * @param directionMoving - direction that we are moving
     */
	private void visitPoint(Point point, String additionalMoves, int directionMoving, Point player){
		char pointChar = board.get(point.y).charAt(point.x);
        if (checkIfPlayerCanGo(point)) //Open space, add to places we can get to
        {
            movablePositions[point.y][point.x] = additionalMoves;
            myPointQ.add(point);
        }
        else if(pointChar == Constants.BOX || pointChar == Constants.BOX_ON_GOAL) //Box, Check to see if box can be moved, if it can make new board state
        {
            checkBox(point, directionMoving, additionalMoves, player);
        }
	}

    /**
     * Checks to see if a box can be moved and moves it if possible
     * @param point - point that the box is at
     * @param directionMoving - direction the player is going to push (towards the box)
     * @param additionalMoves - moves taken to get here from the current state
     */
    private void checkBox(Point point, int directionMoving, String additionalMoves, Point player)
    {
        BoardIterator bi = new BoardIterator(board);
        bi.setLocation(point);
        char twoAway;
        switch(directionMoving)
        {
            case UP:
                twoAway = bi.seeUpByAmount(1);
                Point pointAbove = PointUtil.pointAbove(point);
                
                if (isOptionForBox(twoAway, pointAbove) && !checkIfDynamicDeadlock(pointAbove, UP))
                {
                    moveBox(player, point, pointAbove, additionalMoves);
                }
                break;
            case DOWN:
                twoAway = bi.seeDownByAmount(1);
                Point pointBelow = PointUtil.pointBelow(point);
                if (isOptionForBox(twoAway, pointBelow) && !checkIfDynamicDeadlock(pointBelow, DOWN))
                {
                    moveBox(player, point, pointBelow, additionalMoves);
                }
                break;
            case RIGHT:
                twoAway = bi.seeRightByAmount(1);
                Point pointRight = PointUtil.pointRight(point);
                if (isOptionForBox(twoAway, pointRight) && !checkIfDynamicDeadlock(pointRight, RIGHT))
                {
                    moveBox(player, point, pointRight, additionalMoves);
                }
                break;
            case LEFT:
                twoAway = bi.seeLeftByAmount(1);
                Point pointLeft = PointUtil.pointLeft(point);
                if (isOptionForBox(twoAway, pointLeft) && !checkIfDynamicDeadlock(pointLeft, LEFT))
                {
                    moveBox(player, point, pointLeft, additionalMoves);
                }
                break;
        }
    }

    /**
     * Moves the given box, creates a new board and adds it to child state
     * @param playerSpace - point that the player is in
     * @param boxSpace - space that the box is in
     * @param openSpace - space to push the box
     * @param additionalMoves - The moves taken to get to this point from where the player started this state
     */

    private void moveBox(Point playerSpace, Point boxSpace, Point openSpace, String additionalMoves)
    {
        //Copy the board so we can modify it to make the child state
        @SuppressWarnings("unchecked")
        BoardState childBoard = this.clone();

        childBoard.movesToReachState = movesToReachState + additionalMoves;
        childBoard.parent = this;
        //Make player spot empty, or what it was before
        char playerChar = board.get(playerSpace.y).charAt(playerSpace.x);
        if (playerChar == Constants.PLAYER)
        {
            childBoard.changeChar(playerSpace, Constants.OPEN_SPACE);
        }
        else if (playerChar == Constants.PLAYER_ON_GOAL)
        {
            childBoard.changeChar(playerSpace, Constants.GOAL);
        }
        //Make box spot where the player is
        char boxChar = board.get(boxSpace.y).charAt(boxSpace.x);
        if(boxChar == Constants.BOX)
        {
            childBoard.changeChar(boxSpace, Constants.PLAYER);
        }
        else if (boxChar == Constants.BOX_ON_GOAL)
        {
            childBoard.changeChar(boxSpace, Constants.PLAYER_ON_GOAL);
        }
        //make the open space a box
        char openChar = board.get(openSpace.y).charAt(openSpace.x);
        if (openChar == Constants.OPEN_SPACE  || openChar == Constants.PLAYER)
        {
            childBoard.changeChar(openSpace, Constants.BOX);
        }
        else if (openChar == Constants.GOAL || openChar == Constants.PLAYER_ON_GOAL)
        {
            childBoard.changeChar(openSpace, Constants.BOX_ON_GOAL);
        }

        for (int i=0; i<childBoard.distanceToGoals.size();i++)
        {
            if (childBoard.distanceToGoals.get(i).boxPoint.x == boxSpace.x && childBoard.distanceToGoals.get(i).boxPoint.y == boxSpace.y)
            {
            	childBoard.distanceToGoals.get(i).setBoxPoint(openSpace);
                break;
            }
        }
        childBoard.getHash();
        childBoard.setHeuristicScore();
//        childBoard.printBoardWithParent();
//        childBoard.printDistanceToGoals();
        childStates.add(childBoard);
    }

    //TODO::Refactor - this could just take in a point and find the char inside of here
    /**
     * Checks to see if a box is able to move to this point
     * @param c - char at the point
     * @param p - the point (for checking deadzones)
     * @return
     */
    private boolean isOptionForBox(char c, Point p)
    {
        return c != Constants.WALL && c != Constants.BOX_ON_GOAL && c != Constants.BOX && !deadZones[p.y][p.x];
    }
    private boolean checkIfDynamicDeadlock(Point p, int direction)
    {
    	boolean dynamicDeadlock = false;
    	BoardIterator bIter = new BoardIterator(board);
    	bIter.setLocation(p);
    	if (bIter.currentChar()==Constants.GOAL){
    		return false;
    	}
    	else if (direction == UP)
    	{
    		if (isBlocked(bIter.seeLeft()) && isBlocked(bIter.seeUp()) && isBlocked(bIter.seeMultiDirection(new Point(-1, 1)))
    				|| isBlocked(bIter.seeRight()) && isBlocked(bIter.seeUp()) && isBlocked(bIter.seeMultiDirection(new Point(1, 1))))
			{
				dynamicDeadlock = true;
			}
    	}
    	else if (direction == DOWN)
    	{
    		if (isBlocked(bIter.seeLeft()) && isBlocked(bIter.seeDown()) && isBlocked(bIter.seeMultiDirection(new Point(-1, -1)))
    				|| isBlocked(bIter.seeRight()) && isBlocked(bIter.seeDown()) && isBlocked(bIter.seeMultiDirection(new Point(1, -1))))
			{
				dynamicDeadlock = true;
			}
    	}
    	else if (direction == LEFT)
    	{
    		if ((isBlocked(bIter.seeLeft()) && isBlocked(bIter.seeUp()) && isBlocked(bIter.seeMultiDirection(new Point(-1, 1))))
    				|| isBlocked(bIter.seeLeft()) && isBlocked(bIter.seeDown()) && isBlocked(bIter.seeMultiDirection(new Point(-1, -1))))
			{
				dynamicDeadlock = true;
			}
    	}
    	else if (direction == RIGHT)
    	{
    		if (isBlocked(bIter.seeRight()) && isBlocked(bIter.seeUp()) && isBlocked(bIter.seeMultiDirection(new Point(1, 1)))
    				|| isBlocked(bIter.seeRight()) && isBlocked(bIter.seeDown()) && isBlocked(bIter.seeMultiDirection(new Point(1, -1))))
			{
				dynamicDeadlock = true;
			}
    	}
    	
    	return dynamicDeadlock;
    }
    private boolean isBlocked(char c)
    {
    	if (c == Constants.BOX || c == Constants.BOX_ON_GOAL || c == Constants.WALL)
    	{
    		return true;
    	}
    	else return false;
    }
    /**
     * Finds the child states and adds them to childStates
      */
	public void updateChildStates() {
		Point player = new Point();
		
		BoardIterator bIter = new BoardIterator(board);

		char nextPoint = bIter.nextPoint();
        while (nextPoint!=Constants.TERMINATING_CHAR)
        {
            if (nextPoint==Constants.PLAYER || nextPoint==Constants.PLAYER_ON_GOAL){
                player = new Point(bIter.x, bIter.y);
                break;
            }
            nextPoint = bIter.nextPoint();
        }
		myPointQ.add(player);
        movablePositions[player.y][player.x] = " ";
		while (myPointQ.peek() != null) {
			Point currentPoint = myPointQ.poll();
            visitNeighbors(currentPoint, player);
		}
	}
	
	/**
	 * A static method to calculate the manhattan distance between two points.
	 * 
	 * The manhattan distance should probably be changed for actual moving
	 * distance in a later stage.
	 * 
	 * @param from - Starting point
	 * @param to - Ending point
	 * @return - The manhattan distance between two points
	 */
	public static int calculateDistance(Point from, Point to)
	{
		return Math.abs(from.x - to.x) + Math.abs(from.y - to.y);
	}
	
	/**
	 * Goes through the places on the board, and returns the box positions
	 * 
	 * @param  - None
	 * @return - A vector of the current box-positions
	 */
	public Vector<Point> getBoxPositions()
	{
		Vector<Point> boxPositions = new Vector<Point>();
		BoardIterator bIter = new BoardIterator(board);
		char nextPoint = bIter.nextPoint();
			while (nextPoint!=00)
			{
				if (nextPoint==Constants.BOX || nextPoint==Constants.BOX_ON_GOAL)
                {
					//System.out.println("Box at " + bIter.x + "," + bIter.y);
                    boxPositions.add(new Point(bIter.x, bIter.y));
                }
				nextPoint = bIter.nextPoint();
			}
		return boxPositions;
	}
	/**
	 * Goes through the places on the board, and returns the goal positions
	 * 
	 * @param  - None
	 * @return - A vector of the current goal-positions
	 */
	public Vector<Point> getGoalPositions()
	{
		Vector<Point> goalPositions = new Vector<Point>();
        for(GoalMapping mapping: distanceToGoals)
            for(int y=0; y<mapping.map.length;y++)
                for(int x=0;x<mapping.map[0].length;x++)
                {
                    int nextPoint = mapping.map[y][x];
                    if (nextPoint== 00)
                    {
                        goalPositions.add(new Point(x,y));
                    }
                }
		return goalPositions;
	}

    /**
     * This function creates a 2d array for each goal, with an integer in each place represent
     * how many moves it will take to reach the goal
     *
     * All of these 2d arrays are put to a single vector
      */
    public void createDistanceToGoals()
    {
        //Iterate and find goals
        BoardIterator bIter = new BoardIterator(board);
        char nextPoint = bIter.nextPoint();
        while(nextPoint != Constants.TERMINATING_CHAR)
        {
            if (nextPoint==Constants.GOAL || nextPoint==Constants.BOX_ON_GOAL || nextPoint==Constants.PLAYER_ON_GOAL)
            {
                //When we've found a goal, create a map
                int[][] newMap = createGoalMap(new Point(bIter.x, bIter.y));
                GoalMapping goalMapping = new GoalMapping();
                goalMapping.map = newMap;
                goalMapping.goalPoint = new Point(bIter.x, bIter.y);
                distanceToGoals.add(goalMapping);
            }
            nextPoint = bIter.nextPoint();
        }    
        //Initialize to -1 or max val?
        //Breadth-First Search to find distances to points on map
    }

    /**
     * Helper function of createDistanceToGoals, creates a single map
     */
    //Todo:Add back in logic that if you are on another goal, it's ok
    private int[][] createGoalMap(Point point)
    {
        int nonReachable = Integer.MAX_VALUE;//99; //Integer.MAX_VALUE; //99 for clean output, MAXVAL for submitting
        int[][] newMap = new int[board.size()][longestStringLength];
        //Initialize
        for(int i=0;i<board.size();i++)
            for(int j=0;j<longestStringLength;j++)
                newMap[i][j] = nonReachable;

        newMap[point.y][point.x] = 0;
        //Breadth First Search on this
        Queue<Point> queue = new LinkedList<Point>();
        queue.add(point);
        while(!queue.isEmpty())
        {
            Point currPoint = queue.poll();
            if(isNonWall(currPoint))
            {
                int currPointVal = newMap[currPoint.y][currPoint.x];
                Point tempPoint;
                //Add Up
                tempPoint = new Point(currPoint.x, currPoint.y-1);
                if(!isOutOfBounds(tempPoint) && isNonWall(tempPoint) && newMap[tempPoint.y][tempPoint.x] == nonReachable)
                {
                    newMap[tempPoint.y][tempPoint.x] = currPointVal + 1;
                    queue.add(tempPoint);
                }
                //Add Left
                tempPoint = new Point(currPoint.x-1, currPoint.y);
                if(!isOutOfBounds(tempPoint) && isNonWall(tempPoint) && newMap[tempPoint.y][tempPoint.x] == nonReachable)
                {
                    newMap[tempPoint.y][tempPoint.x] = currPointVal + 1;
                    queue.add(tempPoint);
                }
                //Add Right
                tempPoint = new Point(currPoint.x+1, currPoint.y);
                if(!isOutOfBounds(tempPoint) && isNonWall(tempPoint) && newMap[tempPoint.y][tempPoint.x] == nonReachable)
                {
                    newMap[tempPoint.y][tempPoint.x] = currPointVal + 1;
                    queue.add(tempPoint);
                }
                //Add Down
                tempPoint = new Point(currPoint.x, currPoint.y+1);
                if(!isOutOfBounds(tempPoint) && isNonWall(tempPoint) && newMap[tempPoint.y][tempPoint.x] == nonReachable)
                {
                    newMap[tempPoint.y][tempPoint.x] = currPointVal + 1;
                    queue.add(tempPoint);
                }
            }
        }
        return newMap;
    }

    private boolean isNonWall(Point point)
    {
        char compare = board.get(point.y).charAt(point.x);
        return !(compare == Constants.WALL || deadZones[point.y][point.x]);
    }


	/**
	 * The function calls the calculate distance function for each goal/box combination
	 * to map the two closest ones together.
	 * 
	 * Would preferably be changed for some sort of bipartite matching algorithm for better
	 * precision on the overall calculation of distances, instead of this greedy solution
	 * which is not optimal.
	 * 
	 * @return - An array of [goal][box] mappings
	 */
    //TODO: Im not sure this always assigns all boxes
	public void getGoalMapping()
	{
		// These two calls should probably be replaced with more dynamic position-finding
		// methods in a later stage, since it is unneccessary to fully recalculate them for 
		// every mapping.
		Vector<Point> goalPositions = getGoalPositions();
		Vector<Point> boxPositions = getBoxPositions();
		
		Point[][] goalToBoxMapping = new Point[boxPositions.size()][2];
        int counter=0;
        Vector<Boolean> boxAssigned = new Vector<Boolean>();
        for(int i=0;i<boxPositions.size();i++)
        {
            boxAssigned.add(false);
        }
		for (GoalMapping mapping : distanceToGoals)
		{
			int smallestDistance = -1;
			Point closestBox = null;
			for (Point box : boxPositions)
			{
				int distance = mapping.map[box.y][box.x];

				int iob = boxPositions.indexOf(box);
				if ((smallestDistance == -1 || distance < smallestDistance) && !boxAssigned.get(iob))
				{
					smallestDistance = distance;
					closestBox = box;
				}
			}
			mapping.setBoxPoint(closestBox);
			counter++;
            boxAssigned.set(boxPositions.indexOf(closestBox), true);
            if (counter >= boxPositions.size())
            	break;
		}

		goalMapping = goalToBoxMapping;

	}

    /**
     * Find the places that are deadzones on a board
     * ######
     * # <---DeadZone
     * # <---DeadZone
     * # <---DeadZone
     * #########
     * This is places that if a box goes, we are definitely in a deadlock and it is definitely not a solution.
     *
     * @return - Points (x,y coordinates) of positions that are certain deadlocks if a box is on them.
     */
    //Remember: Don't call a goal a deadzone
    public void findDeadZones() {

        Boolean[] walls = new Boolean[4];
        final int wallUpIndex = 0;
        final int wallRightIndex = 1;
        final int wallDownIndex = 2;
        final int wallLeftIndex = 3;


        BoardIterator iterator = new BoardIterator(board);
        BoardIterator iteratorHelper = new BoardIterator(board);
        do {
            boolean deadzoneFound = false;
            for (int k=0;k<4;k++)
            {
                walls[k] = false;
            }
            if(iterator.currentChar() != Constants.WALL)
            {
                //Check neighbors to see if there is a wall
                if(iterator.seeUp() == Constants.WALL)
                {
                    walls[wallUpIndex] = true;
                }
                if(iterator.seeDown() == Constants.WALL)
                {
                    walls[wallDownIndex] = true;
                }
                if(iterator.seeRight() == Constants.WALL)
                {
                    walls[wallRightIndex] = true;
                }
                if(iterator.seeLeft() == Constants.WALL)
                {
                    walls[wallLeftIndex] = true;
                }

                //Check to see if we are in a corner
                for(int k=0;k<3;k++)
                {
                    if(k == 3)
                    {
                        if(walls[3] && (walls[0]||walls[2]))
                        {
                            addDeadZone(new Point(iterator.x,iterator.y));
                            deadzoneFound = true;
                        }
                    }
                    else if(k == 0)
                    {
                        if(walls[0] && (walls[3]||walls[1]))
                        {
                            addDeadZone(new Point(iterator.x,iterator.y));
                            deadzoneFound = true;
                        }
                    }
                    else
                    {
                        if(walls[k] && (walls[k+1] || walls[k-1]))
                        {
                            addDeadZone(new Point(iterator.x,iterator.y));
                            deadzoneFound = true;
                        }
                    }
                } //End for loop checking if in corner
                if(deadzoneFound)
                    continue;
                 //Check to see if we are along wall
                iteratorHelper.setLocation(new Point(iterator.x,iterator.y));
                if(walls[wallUpIndex] == true
                        && followWall(LEFT, UP, iteratorHelper))
                {
                    addDeadZone(new Point(iterator.x,iterator.y));
                }
                if(walls[wallDownIndex] == true
                        && followWall(LEFT, DOWN, iterator))
                {
                    addDeadZone(new Point(iterator.x,iterator.y));
                }
                if(walls[wallRightIndex] == true
                        && followWall(UP, RIGHT, iterator))
                {
                    addDeadZone(new Point(iterator.x,iterator.y));
                }
                if(walls[wallLeftIndex] == true
                        && followWall(UP, LEFT, iterator))
                {
                    addDeadZone(new Point(iterator.x,iterator.y));
                }
            }
        } while(iterator.nextPoint() != Constants.TERMINATING_CHAR);

    }

    private void changeChar(Point point, char newChar)
    {
        StringBuilder stringBuilder = new StringBuilder(board.get(point.y));
        stringBuilder.setCharAt(point.x, newChar);
        board.set(point.y, stringBuilder.toString());

    }
    /**
     * 
     * @param from - The point from which we want to look for a path
     * @param to	- The point we want to reach
     * @return		- An array of boardpoints that is the path we should use (if there are any), otherwise empty
     */
    public Vector<BoardPoint> isReachable(Point from, Point to)
    {
    	Vector<BoardPoint> path = new Vector<BoardPoint>();
    	Vector<BoardPoint> frontier = new Vector<BoardPoint>();				// Priority queue of unexplored nodes, ordered by total cost
    	BoardPoint startPoint = new BoardPoint(from, null, this);
    	startPoint.setG_Score(0);
    	startPoint.setF_Score(calculateDistance(from, to));
    	
    	frontier.add(startPoint);
    	HashMap<Point, BoardPoint> explored = new HashMap<Point, BoardPoint>();
    	while (!frontier.isEmpty())
    	{
    		BoardPoint current = frontier.get(0);
    		if (current.x == to.x && current.y == to.y)
    		{
    			return current.reconstructPath();
    		}
    		frontier.remove(current);
    		explored.put(current.toPoint(), current);
    		for (Point neighbour : current.getNeighbours())
    		{
    			int tentative_gScore = current.getG_Score() + 1;
    			int tentative_fScore = tentative_gScore + calculateDistance(neighbour, to);
    			BoardPoint bp = explored.get(neighbour);
    			if (bp != null && tentative_fScore > bp.getF_score())
    			{
    				continue;
    			}
    			else if (!frontier.contains(bp) || tentative_fScore < bp.getF_score())
    			{
    				if (bp == null) 
    				{
    					bp = new BoardPoint(neighbour, current, this);
    				}
    				else
    				{
    					bp.setParent(current);
    				}
    				bp.setG_Score(tentative_gScore);
    				bp.setF_Score(tentative_fScore);
    				if (!frontier.contains(bp))
    				{
    					frontier.add(bp);
    				}
    			}
    		}
    		Collections.sort(frontier);
    	}
    	
    	return path;
    }

    private void addDeadZone(Point point)
    {
    	if (board.get(point.y).charAt(point.x) != Constants.GOAL)
    		deadZones[point.y][point.x] = true;
    }

    //Follows a wall to see if the point was a deadzone
    // If it was a deadzone return true
    // Else false is returned
    private boolean followWall(int directionToFollow, int directionToLook, BoardIterator iterator)
    {
        //Move left and right
        if(directionToFollow == RIGHT || directionToFollow == LEFT)
        {
            int i=1;
            int movingDirection = RIGHT;
            while(true)
            {
                char compareChar = movingDirection == RIGHT ? iterator.seeRightByAmount(i) : iterator.seeLeftByAmount(i);
                if(compareChar == Constants.WALL
                        || compareChar == Constants.TERMINATING_CHAR
                        || compareChar == Constants.DEAD_ZONE)
                {
                    i=0;
                    if (movingDirection == RIGHT)
                    {
                        movingDirection = LEFT;
                    }
                    else
                    {
                        return true;
                    }
                }
                else if (compareChar == Constants.GOAL || compareChar == Constants.PLAYER_ON_GOAL || compareChar == Constants.BOX_ON_GOAL)
                {
                	return false;
                }
                int checkY = directionToLook == UP ? -1 : 1;
                int checkX = movingDirection == RIGHT ? i : -i;
                //Check to make sure we are still on the wall
                char secondaryChar = iterator.seeMultiDirection(new Point(checkX, checkY));

                if (secondaryChar != Constants.WALL)
                {
                   return false;
                }
                i++;
            }
        }
        else
        {
            int i=0;
            int movingDirection = UP;

            while(true)
            {
                //Check Up and Down
                char compareChar = movingDirection == UP ? iterator.seeUpByAmount(i) : iterator.seeDownByAmount(i);
                if(compareChar == Constants.WALL
                        || compareChar == Constants.TERMINATING_CHAR
                        || compareChar == Constants.DEAD_ZONE)
                {

                    i=0;
                    if (movingDirection == UP)
                        movingDirection = DOWN;
                    else
                    {
                        return true;
                    }

                }
                else if (compareChar == Constants.GOAL || compareChar == Constants.PLAYER_ON_GOAL || compareChar == Constants.BOX_ON_GOAL)
                {
                	return false;
                }
                int checkX = directionToLook == RIGHT ? 1 : -1;
                int checkY = movingDirection == DOWN ? i : -i;
                //Check to make sure we are still on the wall
                char secondaryChar = iterator.seeMultiDirection(new Point(checkX, checkY));
                if (secondaryChar != Constants.WALL && secondaryChar != Constants.DEAD_ZONE)
                {
                    return false;
                }
                i++;
            }
        }
    }

    public void printBoard()
    {
        for (int y=0; y<board.size();y++)
        {
            for(int x=0; x<board.get(y).length(); x++)
            {
                System.out.print(board.get(y).charAt(x));
            }
            System.out.println();
        }
    }

    public void printDistanceToGoals()
    {
        for(GoalMapping mapping : distanceToGoals)
        {
            for (int y=0; y<board.size();y++)
            {
                for(int x=0; x<board.get(y).length(); x++)
                {
                    System.out.print(String.format("%02d",mapping.map[y][x]) + " ");
                }
                System.out.println();
            }
            System.out.println();
            System.out.println();
        }
    }
    public void printBoardWithParent()
    {
    	
    	int maxLen = 0;
    	for (String s : board)
    	{
    		if (s.length() > maxLen)
    			maxLen = s.length();
    	}
    	
        for (int y=0; y<board.size();y++)
        {
        	String s = "";
            for(int x=0; x<board.get(y).length(); x++)
            {
                s+=board.get(y).charAt(x);
            }
            if (s.length() < maxLen)
            {
            	for (int i = 0; i<= maxLen-s.length(); i++)
            	{
            		s+=" ";
            	}
            }
            s+="    ";
            if (parent != null)
            {
	            for (int x=0; x<parent.board.get(y).length(); x++)
	            {
	            	s+=parent.board.get(y).charAt(x);
	            }
            }
            s += "Child score-- " + score + "   ";
            if (parent != null)
                s+= "Parent Score--" + parent.score;
            //for(int x=0; x<board.get(y).length(); x++)
            System.out.println(s);
        }
        System.out.println();
        System.out.println();
        System.out.println();
    }


    public boolean isSameBoard(BoardState compareBoard)
    {
       BoardIterator bi1 = new BoardIterator(this.board);
       BoardIterator bi2 = new BoardIterator(compareBoard.board);

       while(true)
       {
         char char1 = bi1.nextPoint();
         char char2 = bi2.nextPoint();
         if (char1 == Constants.TERMINATING_CHAR && char2 == Constants.TERMINATING_CHAR)
             return true;
         if (char1 != char2)
             return false;
       }
    }
	public Vector<BoardState> reconstructPath() {
		Vector<BoardState> path = new Vector<BoardState>();
		BoardState bp = this;
		path.add(bp);
		while (bp.parent != null)
		{
			bp = bp.parent;
			path.add(bp);
		}
		Collections.reverse(path);
		return path;
	}
    /**
     * 
     * @return - all BOX replaced with OPEN_SPACE, and all GOAL set to BOX_ON_GOAL
     */
    public BoardState getGoalState()
    {
    	BoardState goalState = new BoardState(board, null);
    	for (String s : goalState.board)
    	{	
			s.replace(Constants.BOX, Constants.OPEN_SPACE);
			s.replace(Constants.GOAL, Constants.BOX_ON_GOAL);
			s.replace(Constants.PLAYER, Constants.OPEN_SPACE);
    	}
    	return goalState;
    }
    
    /**
     * 
     * @return - The board, but with player replaced by open space
     */
    public Vector<String> getBoardWithoutPlayer()
    {
    	@SuppressWarnings("unchecked")
    	Vector<String> boardWithoutPlayer = (Vector<String>) board.clone();
    	for (int i = 0; i < boardWithoutPlayer.size(); i++)
    	{
    		boardWithoutPlayer.set(i, boardWithoutPlayer.get(i).replace(Constants.PLAYER, Constants.OPEN_SPACE));
    		boardWithoutPlayer.set(i, boardWithoutPlayer.get(i).replace(Constants.PLAYER_ON_GOAL, Constants.GOAL));
    	}
    	return boardWithoutPlayer;
    }
    public void setGScore(int newScore)
    {
    	gScore = newScore;
    }
    public int getGScore()
    {
    	return gScore;
    }
    public void setFScore(int newScore)
    {
    	fScore = newScore;
    }
    public int getFScore()
    {
    	return fScore;
    }
    public void setParent(BoardState newParent)
    {
    	parent = newParent;
    }
	public int compareTo(BoardState bp)
	{
//		if (getFScore() > bp.getFScore())
//		{
//			return 1;
//		}
//		else if (getFScore() < bp.getFScore())
//		{
//			return -1;
//		}
//		else
//		{
//			if (getGScore() < bp.getGScore())
//			{
//				return 1;
//			}
//			else if (getGScore() > bp.getGScore())
//			{
//				return -1;
//			}
//			else return 0;
//		}

	    return score < bp.score ? -1 : 1;

	}

    private boolean isOutOfBounds(Point point)
    {
        return point.y < 0 || point.y >= board.size() || point.x < 0 || point.x >= board.get(point.y).length();
    }

    public boolean isFinalState()
    {
        for(GoalMapping mapping : distanceToGoals)
        {
            Point temp = mapping.boxPoint;
            if(board.get(temp.y).charAt(temp.x) != Constants.BOX_ON_GOAL)
                return false;
        }
        return true;
    }
}
