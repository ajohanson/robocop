import java.awt.*;
import java.util.Arrays;
import java.util.Vector;

/**
 * Created with IntelliJ IDEA.
 * User: alec
 * Date: 10/8/13
 * Time: 1:28 AM
 * To change this template use File | Settings | File Templates.
 */
public class BoardStateTest {

    public void runTests() {
        testDeadZones();
        //testIsReachable();
    }
    
    public void testDeadZones()
    {

        testCorners();
        testWalls();
        testMap31();
        testMap31Modified();
        testIsSameBoard();
    }
    private void testCorners()
    {

        //Test corners
        Vector<String> start = new Vector<String>();
        start.add("####");
        start.add("#  #");
        start.add("#  #");
        start.add("####");

        Vector<String> goal = new Vector<String>();
        goal.add("####");
        goal.add("#!!#");
        goal.add("#!!#");
        goal.add("####");

        BoardState bs = new BoardState(start, null);

        Boolean[][] correctResult = new Boolean[3][3];

        for(int i=0;i<correctResult.length;i++)
            for(int j=0;j<correctResult[i].length;j++)
                correctResult[i][j] = false;
        correctResult[1][1] = true;
        correctResult[2][1] = true;
        correctResult[2][2] = true;
        correctResult[1][2] = true;

        for(int i=0;i<correctResult.length;i++)
            for(int j=0;j<correctResult[i].length;j++)
            {
                if(bs.deadZones[i][j] != correctResult[i][j])
                {
                    System.out.println("[FAIL] - BoardStateFindDeadZones - Corners");
                    return;
                }

            }
        System.out.print("[PASS]");
        System.out.println(" - BoardStateFindDeadZones - Corners");
    }
    private void testWalls()
    {

        Vector<String> start = new Vector<String>();
        start.add("#####");
        start.add("#   #");
        start.add("#   #");
        start.add("#   #");
        start.add("#   #");
        start.add("#####");

        Vector<String> goal = new Vector<String>();
        goal.add("#####");
        goal.add("#!!!#");
        goal.add("#! !#");
        goal.add("#! !#");
        goal.add("#!!!#");
        goal.add("#####");

        BoardState bs = new BoardState(start, null);

        Boolean[][] correctResult = new Boolean[5][4];

        for(int i=0;i<correctResult.length;i++)
            for(int j=0;j<correctResult[i].length;j++)
                correctResult[i][j] = false;
        correctResult[1][1] = true;
        correctResult[2][1] = true;
        correctResult[3][1] = true;
        correctResult[4][1] = true;
        correctResult[4][2] = true;
        correctResult[4][3] = true;
        correctResult[1][2] = true;
        correctResult[1][3] = true;
        correctResult[2][3] = true;
        correctResult[3][3] = true;


        for(int i=0;i<correctResult.length;i++)
            for(int j=0;j<correctResult[i].length;j++)
            {
                if(bs.deadZones[i][j] != correctResult[i][j])
                {
                    System.out.println("[FAIL] - BoardStateFindDeadZones - Along walls");
                    return;
                }

            }
        System.out.print("[PASS]");
        System.out.println(" - BoardStateFindDeadZones - Along walls");
    }

    public void testMap31()
    {

        Vector<String> start = new Vector<String>();
        start.add("  #####");
        start.add("  #   #");
        start.add("  #   #");
        start.add("### * ###");
        start.add("#  *.*  #");
        start.add("#  *.*  #");
        start.add("#  *.*  #");
        start.add("###$$$###");
        start.add("  # @ #");
        start.add("  #####");

        BoardState bs = new BoardState(start, null);
        Boolean[][] correctResult = new Boolean[10][9];

        for(int i=0;i<correctResult.length;i++)
            for(int j=0;j<correctResult[i].length;j++)
                correctResult[i][j] = false;

        correctResult[1][3] = true;
        correctResult[1][4] = true;
        correctResult[1][5] = true;
        correctResult[4][1] = true;
        correctResult[5][1] = true;
        correctResult[6][1] = true;
        correctResult[4][7] = true;
        correctResult[5][7] = true;
        correctResult[6][7] = true;
        correctResult[8][3] = true;
        correctResult[8][4] = true;
        correctResult[8][5] = true;
        //These come in as deadzones but don't matter either way
        correctResult[2][1] = true;
        correctResult[8][1] = true;


        for(int i=0;i<correctResult.length;i++)
            for(int j=0;j<correctResult[i].length;j++)
            {
                if(bs.deadZones[i][j] != correctResult[i][j])
                {
                    System.out.println("[FAIL] - BoardStateFindDeadZones - Map31");
                    return;
                }

            }
        System.out.print("[PASS]");
        System.out.println(" - BoardStateFindDeadZones - Map31");
    }

    private void testMap31Modified()
    {

        Vector<String> start = new Vector<String>();
        start.add("  #####");
        start.add("  #   #");
        start.add("  #   #");
        start.add("### * ###");
        start.add("#  *.*  #");
        start.add("#  *.*  #");
        start.add("#  ***  #");
        start.add("###$@$###");
        start.add("  #   #");
        start.add("  #####");

        BoardState bs = new BoardState(start, null);
        Boolean[][] correctResult = new Boolean[10][9];

        for(int i=0;i<correctResult.length;i++)
            for(int j=0;j<correctResult[i].length;j++)
                correctResult[i][j] = false;

        correctResult[1][3] = true;
        correctResult[1][4] = true;
        correctResult[1][5] = true;
        correctResult[4][1] = true;
        correctResult[5][1] = true;
        correctResult[6][1] = true;
        correctResult[4][7] = true;
        correctResult[5][7] = true;
        correctResult[6][7] = true;
        correctResult[8][3] = true;
        correctResult[8][4] = true;
        correctResult[8][5] = true;
        //These come in as deadzones but don't matter either way
        correctResult[2][1] = true;
        correctResult[8][1] = true;


        for(int i=0;i<correctResult.length;i++)
            for(int j=0;j<correctResult[i].length;j++)
            {
                if(bs.deadZones[i][j] != correctResult[i][j])
                {
                    System.out.println("[FAIL] - BoardStateFindDeadZones - Map31Modified");
                    return;
                }

            }
        System.out.print("[PASS]");
        System.out.println(" - BoardStateFindDeadZones - Map31Modified");
    }

    private void testIsSameBoard()
    {
        Vector<String> boardOne = new Vector<String>();
        boardOne.add("  #####");
        boardOne.add("  #   #");
        boardOne.add("  #   #");
        boardOne.add("### * ###");
        boardOne.add("#  *.*  #");
        boardOne.add("#  *.*  #");
        boardOne.add("#  *.*  #");
        boardOne.add("###$$$###");
        boardOne.add("  # @ #");
        boardOne.add("  #####");

        Vector<String> boardTwo = new Vector<String>();
        boardTwo.add("  #####");
        boardTwo.add("  #   #");
        boardTwo.add("  #   #");
        boardTwo.add("### * ###");
        boardTwo.add("#  *.*  #");
        boardTwo.add("#  *.*  #");
        boardTwo.add("#  *.*  #");
        boardTwo.add("###$$$###");
        boardTwo.add("  # @ #");
        boardTwo.add("  #####");

        Vector<String> boardThree = new Vector<String>();
        boardThree.add("  #####");
        boardThree.add("  #   #");
        boardThree.add("  #   #");
        boardThree.add("### * ###");
        boardThree.add("#  *.*  #");
        boardThree.add("#  *.*  #");
        boardThree.add("#  *.*  #");
        boardThree.add("###$$$###");
        boardThree.add("  #  @#");
        boardThree.add("  #####");

        BoardState one = new BoardState(boardOne, null);
        BoardState two = new BoardState(boardTwo, null);
        BoardState three = new BoardState(boardThree, null);
        if (!one.isSameBoard(two))
            System.out.println("[FAIL] - CompareBoards - Same");
        else
            System.out.println("[PASS] - CompareBoards - Same");

        if (one.isSameBoard(three))
            System.out.println("[FAIL] - CompareBoards - Different");
        else
            System.out.println("[PASS] - CompareBoards - Different");




    }
    public void testIsReachable()
    {
        Vector<String> start = new Vector<String>();
        /*
        start.add("  #####");
        start.add("  #   #");
        start.add("  #   #");
        start.add("### * ###");
        start.add("#  *.*  #");
        start.add("#  *.*  #");
        start.add("#  ***  #");
        start.add("###$@$###");
        start.add("  #   #");
        start.add("  #####");
        */
        
        // Map 67 
        start.add("                       ####");
        start.add("######              ####  #");
        start.add("#  # ################     ##");
        start.add("#    #  #     ##....       #");
        start.add("#  #  $  $  # ###...   ##  #");
        start.add("#  $  #  # $  ####..#   ..##");
        start.add("#  # $  $  #  #####.  #  .#");
        start.add("#    #  #  $  ######@##  ##");
        start.add("## $   $   ## #######     #");
        start.add("# # $ #  $ # #######    ##");
        start.add("#   # $  # # ######    ##");
        start.add("#     # $  # #####    ##");
        start.add("########## # ####    ##");
        start.add("#          # ###    ##");
        start.add("#            ##    ##");
        start.add("## ###########    ##");
        start.add("#  #             ##");
        start.add("#           #   ##");
        start.add("##########  #####");
        start.add("         ####");

        BoardState bs = new BoardState(start, null);
        //Point[][] GoalMapping = bs.getGoalMapping();

    	/*
        for (int i = 0; i < GoalMapping.length; i++) {
        	Point goal = GoalMapping[i][0];
        	Point box = GoalMapping[i][1];
        	System.out.println("For box at ("+box.x+", "+box.y+") to goal ("+goal.x+", "+goal.y+") :");
        	Vector<BoardPoint> path = bs.isReachable(box, goal);
        	for (BoardPoint bp : path)
        	{
        		System.out.println(bp+" ");
        	}
        	System.out.println();
        }
        */
        
    }
}
